#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse
import subprocess

from daiquiri.app import run_server
from daiquiri.resources import utils


def main():
    """Runs REST server with CLI configuration
    """
    root = os.path.dirname(__file__)
    # Currently not accepting additional resource folders from CLI
    resource_folder = os.path.join(root, "resources")
    # Default hardware folder (TODO: what is this for?)
    hardware_folder = os.path.join(root, "resources", "external")
    if not os.path.isdir(hardware_folder):
        hardware_folder = ""

    parser = argparse.ArgumentParser(description="Daiquri REST server")
    parser.add_argument(
        "--static-folder",
        dest="static_folder",
        default="static.default",
        help="Web server static folder (static.* refers to server resource)",
    )
    parser.add_argument(
        "--hardware-folder",
        dest="hardware_folder",
        default=hardware_folder,
        help="Hardware folder",
    )
    parser.add_argument(
        "-p", "--port", dest="port", default=8080, help="Web server port", type=int
    )
    args = parser.parse_args()
    run_server(
        resource_folders=[resource_folder],
        implementors="daiquiri_nscope.implementors",
        static_folder=args.static_folder,
        hardware_folder=args.hardware_folder,
        port=args.port,
        static_url_path="/",
    )


def wsgi():
    root = os.path.dirname(__file__)
    resource_folder = os.path.join(root, "resources")
    utils.add_resource_root(resource_folder)

    config = utils.load_config("config.yml")

    args = [
        "uwsgi",
        "--gevent",
        "1000",
        "--http-websockets",
        "--master" "--wsgi-file",
        "wsgi.py",
        "--callable",
        "app",
        "--stats",
        ":9031",
        "--stats-http",
        "--pyargv",
        f"--resource-folders='{resource_folder}' --implmentors='daiquiri_nscope.implementors'",
    ]

    if config.get("ssl"):
        crt = utils.get_resource("certs", config["ssl_cert"])
        key = utils.get_resource("certs", config["ssl_key"])

        args.append("--https")
        args.append(f":8080,{crt},{key}")

    subprocess.call(args)


if __name__ == "__main__":
    main()
