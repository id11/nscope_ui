import time
import traceback

from bliss import current_session
from daiquiri.core.components import ComponentActor


class Upload_ErrorActor(ComponentActor):
    name = "upload_error"

    def method(self, *args, **kwargs):
        """Actor to process actor errors externally

        Kwargs:
            actid (str): The actor id
            actor (ComponentActor): The actor raising the exception
            exception (Exception): The exception the actor raised

        """
        tb = "".join(traceback.format_tb(kwargs['exception'].__traceback__))
        elogbook = current_session.scan_saving.elogbook
        elogbook.send_message(
            f"Actor {kwargs['actid']} failed with exception {str(kwargs['exception'])}\nTraceback:\n{tb}",
            msg_type="error",
        )
