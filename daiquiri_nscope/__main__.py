# -*- coding: utf-8 -*-

# Allows running the REST server with "python -m daiquiri_daiquiri_nscope"
from daiquiri_daiquiri_nscope.app import main

main()
