# Development

## Install

Editable install (needed to load actor implementors, see `daiquiri_daiquiri_nscope/resources/config/config.yml#implementors`)

```
python -m pip install --no-deps -e .
```

## Run REST server

Start server with console script

```
daiquiri-server-daiquiri_nscope --static-folder=... --hardware-folder=...
```

or execute the python module

```
python -m daiquiri_daiquiri_nscope --static-folder=... --hardware-folder=...
```

The server configuration can be found in `daiquiri_daiquiri_nscope/resources/config`.
