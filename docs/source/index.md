# Daiquiri Documentation

## Start REST server

```
daiquiri-server-daiquiri_id11
```

 * **static-folder**: web client static pages
 * **hardware-folder**: hardware repository

Server resources will be searched for in `daiquiri_daiquiri_id11.resources` followed by `daiquiri.resources`.
